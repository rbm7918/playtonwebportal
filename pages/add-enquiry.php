<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>PlayTon</title>
<!-- Bootstrap Core CSS -->
<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="../dist/css/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

<!-- main stylesheet-->
<link rel='stylesheet' type='text/css' href='../dist/css/main.css'>

<!-- Custom Fonts -->
<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div id="wrapper">
<?php include_once('include/header.php');?>
            <?php include_once('include/navigation.php');?>
<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Add Enquiry</h1>
      <div class="clearfix"></div>
      <div class="col-md-4">
        <div class="row">
          <form role="form">
            <fieldset>
              <div class="form-group">
                <!-- <div class="Raund center-block"> <a onclick="getFile()" style="text-decoration:none;" href="javascript:;"> <img src="../images/Raund.svg" width="228" height="228" alt=""/> </a>
                  <div style="height: 0px;width:0px; overflow:hidden;">
                    <input type="file" value="upload" name="image" id="upfile">
                  </div>
                </div> -->
                <div class="clearfix"></div>
                <br/>
                <input class="form-control"  placeholder="Contact No" name="email" type="email" autofocus>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Full Name" name="email" type="email" autofocus>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Email Id" name="email" type="email" autofocus>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Player Location" name="number" type="email" autofocus>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Source" name="password" type="password" autofocus >
              </div>
              <div>
              <label>Enquiry for</label>
              <select class="form-control">
                                               
                                                <option>Booking</option>
                                                <option>Tranning</option>
             </select>
              </div>
              <div>
              <label>Select Sport</label>
              <select class="form-control">
                                                <option>cricket</option>
                                                <option>Football</option>
                                                <option>Hockey</option>
              </select>
              </div>
              <div>
              <div class="control-group">
        <label for="date-picker-2" class="control-label">Select Date</label>
        <div class="controls">
            <div class="input-group">
                <input id="date-picker-2" type="text" class="date-picker form-control" />
                <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
        </div>
    </div>
            </div>
            <div>
                <label>Select Sport</label>
              <select class="form-control">
              <option>cricket</option>
              <option>Football</option>
               <option>Hockey</option>
              </select>
            </div>
             <div class="checkbox vertical-center">
                 <label>
                     <input type="checkbox" value="">Send SMS
                 </label>
             </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
      <!--col-md-4-->
      <!-- <div class="clearfix"></div>
      <div class="form-group">
        
        <div class="row">
          <div class="col-sm-2">
            <div class="img-responsive"><img src="../images/Academy.jpg" width="100%" height="auto" alt=""/></div>
            <a href="#">Delete Or Edit</a> </div>
          <div class="col-sm-2">
            <div class="img-responsive"><img src="../images/Academy.jpg" width="100%" height="auto" alt=""/></div>
            <a href="#">Delete Or Edit</a> </div>
          <div class="col-sm-2">
            <div class="img-responsive"><img src="../images/Academy.jpg" width="100%" height="auto" alt=""/></div>
            <a href="#">Delete Or Edit</a> </div>
          <div class="col-sm-2">
            <div class="img-responsive"><img src="../images/Academy.jpg" width="100%" height="auto" alt=""/></div>
            <a href="#">Delete Or Edit</a> </div>
          <div class="col-sm-2">
            <div class="img-responsive"><img src="../images/Academy.jpg" width="100%" height="auto" alt=""/></div>
            <a href="#">Delete Or Edit</a> </div>
        </div>
      </div> -->
      <div class="clearfix"></div>
      <br/>
      <div class="col-md-4">
      <div class="col-md-5">
        <div class="row "> 
          <!-- Change this to a button or input when using this as a form --> 
          <a href="viewEnquiry.php" class="btn btn-lg btn-success btn-block ">Add</a> <br/><br/></div>
      </div>


      <div class="col-md-6">
        <!-- <div class="row btx"> --> 
          <!-- Change this to a button or input when using this as a form --> 
          <a href="" class="btn btn-lg btn-success btn-block ">Add & Book</a> <br/><br/></div>
      <!-- </div> -->
    </div>
      <!-- /.col-lg-12 -->
      <div class="clearfix"></div>
    </div>
    <!-- /.row --> 
  </div>
  <!-- /#page-wrapper --> 
  <div class='clearfix'></div>
		<?php include_once('include/footer.php');?>
</div>
<!-- /#wrapper --> 

<!-- jQuery --> 
<script src="../bower_components/jquery/dist/jquery.min.js"></script> 



<!-- Bootstrap Core JavaScript --> 
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>


<!-- Metis Menu Plugin JavaScript --> 
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script> 

<!-- Morris Charts JavaScript --> 
<script src="../bower_components/raphael/raphael-min.js"></script> 
<script src="../bower_components/morrisjs/morris.min.js"></script> 
<script src="../js/morris-data.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../dist/js/sb-admin-2.js"></script> 
<script>
                function getFile(){
                document.getElementById("upfile").click();
                }
                $("document").ready(function(){
                $("#upfile").change(function() {
                //alert('changed!');
                $("#updateImgFrm").submit();
                });
                $('#profile').addClass('current');
                });
              
//datepicker javascript start// 
 $(".date-picker").datepicker();

$(".date-picker").on("change", function () {
    var id = $(this).attr("id");
    var val = $("label[for='" + id + "']").text();
    $("#msg").text(val + " changed");
});
//date picket javascript end//
</script>
</body>
</html>

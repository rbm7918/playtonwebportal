<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>PlayTon</title>
  <!-- Bootstrap Core CSS -->
  <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- MetisMenu CSS -->
  <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
  <!-- Timeline CSS -->
  <link href="../dist/css/timeline.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
  <!-- Morris Charts CSS -->
  <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
  <!-- main stylesheet-->
  <link rel='stylesheet' type='text/css' href='../dist/css/main.css'>
  <!-- Custom Fonts -->
  <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
    <body>
        <div id="wrapper">
            <?php include_once('include/header.php');?>
            <?php include_once('include/navigation.php');?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Location</h1>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class='col-md-10'>
                                    
                                    <div class="editBanner img-responsive">
                                        <img src="">
                                        <div class="bannertext text-center">
                                            <h4 class='text-center'>TAB TO ADD COVER PHOTO</h4>
                                        </div>
                                        <div class="academyBannerLogo ">
                                            <div class="bannerRound">
                                                <img class="" src="../images/locationlogo.png" alt="">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class='clearfix'></div>
                                    <div class='col-md-12'>
                                        <h2 class='text-center nameacademy'>CTC Kingsturt Academy</h2>
                                    </div>
                                    <div class='col-md=12 '>
                                        <div class='daysName'>
                                            <span>Select Days</span></br></br>
                                            <div class='daysButton'>
                                                <button type="button" class="btn  ButtonActive btn-default">Monday</button>
                                                <button type="button" class="btn bgColorButton btn-default">Tuesday</button>
                                                <button type="button" class="btn bgColorButton btn-default">Wednesday</button>
                                                <button type="button" class="btn bgColorButton btn-default">Thrusday</button>
                                                <button type="button" class="btn bgColorButton btn-default">Friday</button>
                                                <button type="button" class="btn bgColorButton btn-default">Saturday</button>
                                                <button type="button" class="btn bgColorButton btn-default">Sunday</button>
                                                
                                            </div>
                                        </div>
                                        <div class='time'>
                                            <span>Select Time</span></br></br>
                                            <button type="button" class="btn bgColorButtonTime btn-default">00 :00 :00</button>
                                            <button type="button" class="btn bgColorButtonTime btn-default">am</button>
                                        </div>
                                        <div class='selectSport'>
                                            <span>Select sport</span></br>
                                            <div  style='display:inline-block;'>
                                                <!-- <label>Choose Location</label> -->
                                                <select class="form-control">
                                                    <option>Baner</option>
                                                    <option>Hadpsar</option>
                                                    <option>Kothrud</option>
                                                    <option>Aundh</option>
                                                </select>
                                            </div>
                                            <button type="button" class="btn bgColorButtonSport btn-default marginRight">am</button>
                                            <div>
                                                <div class='sportName'>
                                                    <div class='sportImages'>
                                                        <img src='../images/handball.png'>
                                                        <p>Handball</p>
                                                    </div>
                                                </div>
                                                <div class='sportName'>
                                                    <div class='sportImages'>
                                                        <img src='../images/hockey.png'>
                                                        <p>Hockey</p>
                                                    </div>
                                                    
                                                </div>
                                                <div class='sportName'>
                                                    <div class='sportImages'>
                                                        <img src='../images/tableTennis.png'>
                                                        <p>Table Tennis</p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class='col-md-11'>
                                        <div class='academyNav'>
                                            <div class='locationNavigation'>
                                                
                                                <img src='../images/abtUs.png'>
                                                <p>About</p>
                                            </div>
                                            <div class='locationNavigation'>
                                                
                                                <img src='../images/imgGallery.png'>
                                                <p>Image Gallery</p>
                                            </div>
                                            <div class='locationNavigation'>
                                                
                                                <img src='../images/facilities.png'>
                                                <p>Facilities</p>
                                            </div>
                                            <div class='locationNavigation'>
                                                
                                                <img src='../images/locationnav.png'>
                                                <p>Location</p>
                                            </div>
                                            <div class='locationNavigation'>
                                                
                                                <img src='../images/sport.png'>
                                                <p>Sport</p>
                                            </div>
                                            <div class="locationAbout">
                                                <h4>About</h4>
                                                <div class='aboutText'>
                                                    <p></p>
                                                </div>
                                            </div>
                                            <div class='col-md-12 locationAbout'>
                                                <h4 class=''>Images Gallery</h4>
                                                <div class='imageGallery'>
                                                    <span>Tap to Add the Photos</span>
                                                </div>
                                            </div>
                                            <div class='col-md-12 selectFacilitiesox'>
                                              <h4>Tap to Select Facilities</h4>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/Lockerroom.svg">
                                                  </div>
                                                </div><br>
                                                <span>LockerRoom</span>
                                              </div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/washroom.svg">
                                                  </div>
                                                </div><br>
                                                <span>Washroom</span>
                                              </div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/eatery.svg">
                                                  </div>
                                                </div><br>
                                                <span>Eatery</span>
                                              </div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/wifi.svg">
                                                  </div>
                                                </div><br>
                                                <span>Wifi</span>
                                              </div>
                                              <div class='clearfix'></div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/parking.svg">
                                                  </div>
                                                </div><br>
                                                <span>Parking</span>
                                              </div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/firstAid.svg">
                                                  </div>
                                                </div><br>
                                                <span>First Aid</span>
                                              </div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/beverage.svg">
                                                  </div>
                                                </div><br>
                                                <span>Beverage</span>
                                              </div>
                                              <div class='locationFacilities'>
                                                <div class='facilitiesIcon '>
                                                  <div class='backgroungFacilities'>
                                                    <img src="../images/equpipment.svg">
                                                  </div>
                                                </div><br>
                                                <span>Equipment</span>
                                              </div>

                                            </div>
                                            <div class='locationAbout col-md-12'>
                                                <h4 class=''>Full Address</h4>
                                                <div class='Locationaddress'>
                                                    <span>Enter Full Address</span>
                                                    <hr>
                                                    <hr>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class='locationAbout col-md-12'>
                                                <h4 class=''>LocationManager Deatails</h4>
                                                <div class='LocationManagerDeatails'>
                                                    <form role="form ">
                                                        <fieldset>
                                                            <div class="form-group ">
                                                                
                                                                <div class="clearfix"></div>
                                                                <br/>
                                                                <input class="form-control" placeholder="Manager Name" name="email" type="email" autofocus>
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" placeholder="Manager Email Id" name="email" type="email" autofocus>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <input class="form-control" placeholder="Mobile No" name="number" type="email" autofocus>
                                                            </div>
                                                            <div class='col-md-6 passwordGenerate bookbutton'>
                                                                 <button type="button" class="btn btn-default">Generate Password</button>
                                                                 <h5 class=''>8fgRt45</h5>
                                                            </div>
                                                            <br>
                                                            
                                                            <div class='col-md-6  bookbutton'>
                                                                 <button type="button" class="btn btn-default">Send</button>
                                                            </div>

                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class='col-md-6  bookbutton'>
                                                                 <button type="button" class="btn pull-right  SubmitDetials btn-default">Submit</button>
                                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <!-- </div>
                        </div>
                        -->
                        
                        
                        
                        
                        
                        
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
    </div>
	<div class='clearfix'></div>
		<?php include_once('include/footer.php');?>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="../bower_components/raphael/raphael-min.js"></script>
<script src="../bower_components/morrisjs/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<script>
function getFile(){
document.getElementById("upfile").click();
}
$("document").ready(function(){
$("#upfile").change(function() {
//alert('changed!');
$("#updateImgFrm").submit();
});
$('#profile').addClass('current');
});
</script>
</body>
</html>
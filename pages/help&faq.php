<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>PlayTon</title>



<!-- Bootstrap Core CSS -->
<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="../dist/css/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

<!-- main stylesheet-->
<link rel='stylesheet' type='text/css' href='../dist/css/main.css'>

<!-- Custom Fonts -->
<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
<div id="wrapper">
<?php include_once('include/header.php');?>
<?php include_once('include/navigation.php');?>
<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Help and FAQ</h1>
      <div class="clearfix"></div>
      <div class="col-md-12">
        <div class="row">
         <div class='col-md-offset-2 col-md-8'>
          <div class='aboutImg'>
            <img src='../images/help&faq.png'>
		      </div> </div>	</br></br>
          <div class="row margin">
                <div class="col-lg-11">
                    <!-- <div class="panel panel-default"> -->
                        
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                           <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                           <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">What is a free team, and what can I do with this feature?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <h6>The Team Folder<h6>
                                          <p>The team folder is a company- or organization-wide folder that’s automatically shared with all members of a free team. Changes to a file in the team folder will reflect in the work Dropbox of each team member.
                                          All sub-folders in the team folder are automatically shared with your entire free team; however, if you’re working on a project with external collaborators, you can choose to share certain sub-folders with those contributors as well. If you're the owner of a shared folder, you can easily keep company information in one into the team folder from your desktop or move them at dropbox.com.
                                          A separate work and personal Dropbox A separate work and personal Dropbox</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
       
        </div>
      </div>
      
     
     
    
      <!-- /.col-lg-12 -->
      <div class="clearfix"></div>
    </div>
    <!-- /.row --> 
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<div class='clearfix'></div>
		<?php include_once('include/footer.php');?>
</div>
<!-- /#wrapper --> 

<!-- jQuery --> 
<script src="../bower_components/jquery/dist/jquery.min.js"></script> 



<!-- Bootstrap Core JavaScript --> 
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>


<!-- Metis Menu Plugin JavaScript --> 
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script> 

<!-- Morris Charts JavaScript --> 
<script src="../bower_components/raphael/raphael-min.js"></script> 
<script src="../bower_components/morrisjs/morris.min.js"></script> 
<script src="../js/morris-data.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../dist/js/sb-admin-2.js"></script> 
<script>
                function getFile(){
                document.getElementById("upfile").click();
                }
                $("document").ready(function(){
                $("#upfile").change(function() {
                //alert('changed!');
                $("#updateImgFrm").submit();
                });
                $('#profile').addClass('current');
                });
                </script>
</body>
</html>

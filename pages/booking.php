<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PlayTon</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- main stylesheet-->
    <link rel='stylesheet' type='text/css' href='../dist/css/main.css'>
    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
      <?php include_once('include/header.php');?>
            <?php include_once('include/navigation.php');?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">Booking</h1>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="row">
                
                <div class='col-md-6'>
                    <div class=' bookingInfoCancelledBox'>
                    <div class='col-md-2'>
                      <div class='academyLogo '>
                          <div class="imground">
						  <img class="" src="../images/academyLogo.png" alt=""/> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class='col-md-8'>
                      <div class='academyNameInfo'>
                        <strong>Sai Sport Academy </strong><span>Baner</span>
                      </div>
					  <div class='userInfoName'>
                       Rahul More
                     </div>
					 <div class='paymentInfo'>
                       305 <span>Paid Online</span> /<span class='disable'>Cash On Arrival</span>
                     </div><!-- // u can apply the disable class to paid online also  -->
                  </div>
                    <div class='col-md-2'>
                    <div class='sportlogo'>
                         <div class='Ellipse_8 pull-right'>
                    <img src="../images/football.png">
                  <h5>Football</h5></div>
                    </div>
                  </div>
                  <div class='col-md-6 bookingIdInfomartion text-center'>
				  <div class='bookDateInfo col-md-offset-1'>
                       12/45/2016
                     </div>
					 <div class='BooktimeInfo'>
                       02:00pm to 03:00pm
                     </div>
                    <div class='BookingId'>
                        <h5>Booking Id<h5>- <span>INPA0000232603</span>
						</div>
						<div class='userdeatils'>
						<h5>Book By<h5>- <span>Priya Londhe</span>
						</div>
					</div>
					<div class='col-md-10 text-center bookbutton'>
                          <button type="button" class="btn btn-default">Cancel</button>
                    </div>
			 </div> 
			</div>
			
                   
                      <div class='col-md-6'>
                    <div class=' bookingInfoCancelledBox'>
                    <div class='col-md-2'>
                      <div class='academyLogo '>
                          <div class="imground">
						  <img class="" src="../images/academyLogo.png" alt=""/> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class='col-md-8'>
                      <div class='academyNameInfo'>
                        <strong>Sai Sport Academy </strong><span>Baner</span>
                      </div>
					  <div class='userInfoName'>
                       Rahul More
                     </div>
					 <div class='paymentInfo'>
                       305 <span>Paid Online</span> /<span class='disable'>Cash On Arrival</span>
                     </div><!-- // u can apply the disable class to paid online also  -->
                  </div>
                    <div class='col-md-2'>
                    <div class='sportlogo'>
                         <div class='Ellipse_8 pull-right'>
                    <img src="../images/football.png">
                  <h5>Football</h5></div>
                    </div>
                  </div>
                  <div class='col-md-6 bookingIdInfomartion text-center'>
				  <div class='bookDateInfo col-md-offset-1'>
                       12/45/2016
                     </div>
					 <div class='BooktimeInfo'>
                       02:00pm to 03:00pm
                     </div>
                    <div class='BookingId'>
                        <h5>Booking Id<h5>- <span>INPA0000232603</span>
						</div>
						<div class='userdeatils'>
						<h5>Book By<h5>- <span>Priya Londhe</span>
						</div>
					</div>
					<div class='col-md-10 text-center bookbutton'>
                          <button type="button" class="btn btn-default">Cancel</button>
                    </div>
			 </div> 
			</div>
			
			<div class='col-md-6'>
                    <div class=' bookingInfoCancelledBox'>
                    <div class='col-md-2'>
                      <div class='academyLogo '>
                          <div class="imground">
						  <img class="" src="../images/academyLogo.png" alt=""/> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class='col-md-8'>
                      <div class='academyNameInfo'>
                        <strong>Sai Sport Academy </strong><span>Baner</span>
                      </div>
					  <div class='userInfoName'>
                       Rahul More
                     </div>
					 <div class='paymentInfo'>
                       305 <span>Paid Online</span> /<span class='disable'>Cash On Arrival</span>
                     </div><!-- // u can apply the disable class to paid online also  -->
                  </div>
                    <div class='col-md-2'>
                    <div class='sportlogo'>
                         <div class='Ellipse_8 pull-right'>
                    <img src="../images/football.png">
                  <h5>Football</h5></div>
                    </div>
                  </div>
                  <div class='col-md-6 bookingIdInfomartion text-center'>
				  <div class='bookDateInfo col-md-offset-1'>
                       12/45/2016
                     </div>
					 <div class='BooktimeInfo'>
                       02:00pm to 03:00pm
                     </div>
                    <div class='BookingId'>
                        <h5>Booking Id<h5>- <span>INPA0000232603</span>
						</div>
						<div class='userdeatils'>
						<h5>Book By<h5>- <span>Priya Londhe</span>
						</div>
					</div>
					<div class='col-md-10 text-center bookbutton'>
                          <button type="button" class="btn btn-default">Cancel</button>
                    </div>
			 </div> 
			</div>
			
			<div class='col-md-6'>
                    <div class=' bookingInfoCancelledBox'>
                    <div class='col-md-2'>
                      <div class='academyLogo '>
                          <div class="imground">
						  <img class="" src="../images/academyLogo.png" alt=""/> </a>
                              
                            </div>
                        </div>
                    </div>
                    <div class='col-md-8'>
                      <div class='academyNameInfo'>
                        <strong>Sai Sport Academy </strong><span>Baner</span>
                      </div>
					  <div class='userInfoName'>
                       Rahul More
                     </div>
					 <div class='paymentInfo'>
                       305 <span>Paid Online</span> /<span class='disable'>Cash On Arrival</span>
                     </div><!-- // u can apply the disable class to paid online also  -->
                  </div>
                    <div class='col-md-2'>
                    <div class='sportlogo'>
                         <div class='Ellipse_8 pull-right'>
                    <img src="../images/football.png">
                  <h5>Football</h5></div>
                    </div>
                  </div>
                  <div class='col-md-6 bookingIdInfomartion text-center'>
				  <div class='bookDateInfo col-md-offset-1'>
                       12/45/2016
                     </div>
					 <div class='BooktimeInfo'>
                       02:00pm to 03:00pm
                     </div>
                    <div class='BookingId'>
                        <h5>Booking Id<h5>- <span>INPA0000232603</span>
						</div>
						<div class='userdeatils'>
						<h5>Book By<h5>- <span>Priya Londhe</span>
						</div>
					</div>
					<div class='col-md-10 text-center bookbutton'>
                          <button type="button" class="btn btn-default">Cancel</button>
                    </div>
			 </div> 
			</div>
                            <!-- /.row -->
                          </div>
                          <!-- /#page-wrapper -->                 
                        </div>
                      </div>
                    </div>
                  </div>
				  <div class='clearfix'></div>
		<?php include_once('include/footer.php');?>
                </div>
                <!-- /#wrapper -->
                <!-- jQuery -->
                <script src="../bower_components/jquery/dist/jquery.min.js"></script>
                <!-- Bootstrap Core JavaScript -->
                <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
                <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                <!-- Metis Menu Plugin JavaScript -->
                <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
                <!-- Morris Charts JavaScript -->
                <script src="../bower_components/raphael/raphael-min.js"></script>
                <script src="../bower_components/morrisjs/morris.min.js"></script>
                <script src="../js/morris-data.js"></script>
                <!-- Custom Theme JavaScript -->
                <script src="../dist/js/sb-admin-2.js"></script>
                <script>
                function getFile(){
                document.getElementById("upfile").click();
                }
                $("document").ready(function(){
                $("#upfile").change(function() {
                //alert('changed!');
                $("#updateImgFrm").submit();
                });
                $('#profile').addClass('current');
                });
                </script>
              </body>
                
            </html>